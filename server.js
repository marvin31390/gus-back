if (process.env.NODE_ENV !== "production") {
  require("dotenv").config();
}
// get all plugins we needs
const path = require("path");
const mysql = require("mysql");
const express = require("express");
const app = express();
const bcrypt = require("bcrypt");
const passport = require("passport");
const flash = require("express-flash");
const session = require("express-session");
const methodOverride = require("method-override");
const initializePassport = require("./passport-config");
const axios = require("axios");
const fs = require("fs");
app.use(express.static(path.join(__dirname, "public")));
// database connection
const db = mysql.createConnection({
  host: "localhost",

  user: "root",

  password: "AZERty1234",

  database: "test-strapi",
});
db.connect(function (err) {
  if (err) throw err;
  console.log("Connecté à la base de données MySQL!");
});
// users is used to verify the users and the password
const users = [];
// query database to retrieve all users
db.query("SELECT * FROM users", function (err, result) {
  if (err) throw err;
  result.map((element) => {
    verification(element);
    return users;
  });
});
// function which checks the user's email address as well as his password
function verification(rows) {
  users.push(rows);
  initializePassport(
    passport,
    (email) => users.find((user) => user.Email === email),
    (id) => users.find((user) => user.Id === id)
  );
}

// define ejs as our view-engine
app.set("view-engine", "ejs");
// allows you to make post requests
app.use(express.urlencoded({ extended: false }));
// allows you to display error message when the users make wrong email or password
app.use(flash());
// use session to users
app.use(
  session({
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
  })
);
// use passport to authentification
app.use(passport.initialize());
// use of passport to manage sessions
app.use(passport.session());
// lets you use HTTP verbs such as PUT or DELETE in places where the client doesn't support it
app.use(methodOverride("_method"));
// set up root "/" and use checkAuthenticated to check authentification of users
app.get("/", checkAuthenticated, (req, res) => {
 
  const readCategories = JSON.parse(fs.readFileSync("./json/categories.json"));
  const readNumeros = JSON.parse(fs.readFileSync("./json/numeros.json"));
  const readPlaces = JSON.parse(fs.readFileSync("./json/places.json"));

  res.render("index.ejs", {
    name: req.user.Name,
    categories: readCategories,
    numeros: readNumeros,
    places: readPlaces,
  });
});
app.get("/generateJson", checkAuthenticated, (req, res) => {
  const options = {
    method: "GET",
    url: "http://localhost:1337/Categories",
    headers: { "Content-Type": "application/json" },
  };
  axios
    .request(options)
    .then(function (response) {
      let results = [];
      response.data.map((l) => {
        let result = {
          Id: l.id,
          Name: l.Name,
          Markercolor: l.Markercolor,
          Alt: l.Alt,
          Icon: l.Icon,
        };
        results.push(result);
      });
      let data2 = JSON.stringify(results);

      fs.writeFileSync("./json/categories.json", data2);
      //file written successfully
    })
    .catch(function (error) {
      console.error(error);
    });
  const optionsNumeros = {
    method: "GET",
    url: "http://localhost:1337/UsefullNumbers",
    headers: { "Content-Type": "application/json" },
  };
  axios
    .request(optionsNumeros)
    .then(function (response) {
      let results = [];
      response.data.map((l) => {
        let result = {
          Id: l.id,
          Name: l.Name,
          Phone: l.Phone,
          Alt: l.Alt,
          Logo: l.Logo,
          Sector: l.Sector,
        };
        results.push(result);
      });
      let data2 = JSON.stringify(results);

      fs.writeFileSync("./json/numeros.json", data2);
      //file written successfully
    })
    .catch(function (error) {
      console.error(error);
    });
  const optionsPlaces = {
    method: "GET",
    url: "http://localhost:1337/Places",
    headers: { "Content-Type": "application/json" },
  };
  axios
    .request(optionsPlaces)
    .then(function (response) {
      let results = [];
      response.data.map((l) => {
        let result = {
          Id: l.id,
          Category: l.Category,
          Name: l.Name,
          Adress: l.Adress,
          Phone: l.Phone,
          Email: l.Email,
          Description: l.Description,
          Weblink: l.Weblink,
          Latitude: l.Latitude,
          Longitude: l.Longitude,
        };
        results.push(result);
      });
      let data2 = JSON.stringify(results);

      fs.writeFileSync("./json/places.json", data2);
      //file written successfully
    })
    .catch(function (error) {
      console.error(error);
    });
    const optionsLangues = {
      method: "GET",
      url: "http://localhost:1337/Languages",
      headers: { "Content-Type": "application/json" },
    };
    axios
      .request(optionsLangues)
      .then(function (response) {
        let results = [];
        response.data.map((l) => {
          let result = {
            Id: l.id,
            Name: l.Name,
            Image: l.Image,
            Initial: l.Initial,
          };
          results.push(result);
        });
        let data2 = JSON.stringify(results);
  
        fs.writeFileSync("./json/langues.json", data2);
        //file written successfully
      })
      .catch(function (error) {
        console.error(error);
      });
      const optionsWords = {
        method: "GET",
        url: "http://localhost:1337/Words",
        headers: { "Content-Type": "application/json" },
      };
      axios
        .request(optionsWords)
        .then(function (response) {
          let resultsFr = [];
          let resultsEs = [];
          let resultsEn = [];
          let resultsKr = [];
          response.data.map((l) => {
            if(l.Language === "Français") {
              let result = {
                Id: l.id,
                Name: l.Name,
                Role: l.Role,
                Language: l.Language,
                Initial_languages: l.Initial_languages
              };
              resultsFr.push(result);
            }
            else if (l.Language === "Espagnol"){
              let result = {
                Id: l.id,
                Name: l.Name,
                Role: l.Role,
                Language: l.Language,
                Initial_languages: l.Initial_languages
              };
              resultsEs.push(result);
            }
            else if (l.Language === "Anglais") {
            let result = {
              Id: l.id,
              Name: l.Name,
              Role: l.Role,
              Language: l.Language,
              Initial_languages: l.Initial_languages
            };
            resultsEn.push(result);
          }
          else if (l.Language === "Coréen") {
            let result = {
              Id: l.id,
              Name: l.Name,
              Role: l.Role,
              Language: l.Language,
              Initial_languages: l.Initial_languages
            };
            resultsKr.push(result);
          }
          });
          let dataFr = JSON.stringify(resultsFr);
          let dataEs = JSON.stringify(resultsEs);
          let dataEn = JSON.stringify(resultsEn);
          let dataKr = JSON.stringify(resultsKr);
    
          fs.writeFileSync("./json/wordsFr.json", dataFr);
          fs.writeFileSync("./json/wordsEs.json", dataEs);
          fs.writeFileSync("./json/wordsEn.json", dataEn);
          fs.writeFileSync("./json/wordsKr.json", dataKr);
          //file written successfully
        })
        .catch(function (error) {
          console.error(error);
        });
        const optionsSectors = {
          method: "GET",
          url: "http://localhost:1337/Sectors",
          headers: { "Content-Type": "application/json" },
        };
        axios
          .request(optionsSectors)
          .then(function (response) {
            let results = [];
            response.data.map((l) => {
              let result = {
                Id: l.d,
                Name: l.Name,
                Logocomcom: l.Logocomcom,
                Logostructure: l.Logostructure,
              };
              results.push(result);
            });
            let data2 = JSON.stringify(results);
      
            fs.writeFileSync("./json/sectors.json", data2);
            //file written successfully
          })
          .catch(function (error) {
            console.error(error);
          });
  const readCategories = JSON.parse(fs.readFileSync("./json/categories.json"));
  const readNumeros = JSON.parse(fs.readFileSync("./json/numeros.json"));
  const readPlaces = JSON.parse(fs.readFileSync("./json/places.json"));
  const readLangues = JSON.parse(fs.readFileSync("./json/langues.json"));
  const readWords = JSON.parse(fs.readFileSync("./json/words.json"));
  const readSectors = JSON.parse(fs.readFileSync("./json/sectors.json"))

  res.render("index.ejs", {
    name: req.user.Name,
    categories: readCategories,
    numeros: readNumeros,
    places: readPlaces,
  });
});

/* set up root "/categories" and use checkAuthenticated to check authentification of users 
 use axios to get the /Categories of strapi api */

app.get("/categories", checkAuthenticated, (req, res) => {
  const readCategories = JSON.parse(fs.readFileSync("./json/categories.json"));
  res.render("./Categories/categories.ejs", { categories: readCategories, name: req.user.Name});
});
app.get("/categories/create", checkAuthenticated, (req, res) => {
  res.render("./Categories/addCategories.ejs", {name: req.user.Name});
});
app.post("/categories/create", checkAuthenticated, (req, res) => {
  const create = {
    Name: req.body.name,
    Markercolor: req.body.markerColor,
    Alt: req.body.alt,
    Icon: req.body.icon,
  };
  const options = {
    method: "POST",
    url: "http://localhost:1337/Categories/",
    headers: { "Content-Type": "application/json" },
    data: create,
  };

  axios
    .request(options)
    .then(function (response) {
      res.render("./Categories/addCategories.ejs", { data: response.data, name: req.user.Name });
    })
    .catch(function (error) {
      console.error(error);
    });
});
app.get("/updateCategories/:id", checkAuthenticated, (req, res) => {
  const options = {
    method: "GET",
    url: `http://localhost:1337/Categories/${req.params.id}`,
    headers: { "Content-Type": "application/json" },
  };

  axios
    .request(options)
    .then(function (response) {
      res.render("./Categories/updateCategories.ejs", { data: response.data, name: req.user.Name });
    })
    .catch(function (error) {
      console.error(error);
    });
});
app.post("/updateCategories/:id", checkAuthenticated, (req, res) => {
  const update = {
    Name: req.body.name,
    Markercolor: req.body.markerColor,
    Alt: req.body.alt,
    Icon: req.body.icon,
  };
  const options = {
    method: "PUT",
    url: `http://localhost:1337/Categories/${req.params.id}`,
    headers: { "content-Type": "application/json" },
    data: update,
  };
  axios
    .request(options)
    .then(function (response) {
      res.render("./Categories/updateCategories.ejs", { data: response.data, name: req.user.Name});
    })
    .catch(function (error) {
      console.error(error);
    });
});
app.get("/categories/delete/:id", checkAuthenticated, (req, res) => {
  const options = {
    method: "DELETE",
    url: `http://localhost:1337/Categories/${req.params.id}`,
    headers: { "Content-Type": "application/json" },
  };

  axios
    .request(options)
    .then(function (response) {
      res.render("./Categories/deleteCategories.ejs", { data: response.data, name: req.user.Name });
    })
    .catch(function (error) {
      console.error(error);
    });
});
/* set up root "/numeros" and use checkAuthenticated to check authentification of users 
use axios to get /UsefullNumbers of strapi api */

app.get("/numeros", checkAuthenticated, (req, res) => {
  const readNumeros = JSON.parse(fs.readFileSync("./json/numeros.json"));

  res.render("./Numeros/numeros.ejs", { numeros: readNumeros, name: req.user.Name });
});

app.get("/numeros/create", checkAuthenticated, (req, res) => {
  res.render("./Numeros/addNumeros.ejs", {name: req.user.Name});
});
app.post("/numeros/create", checkAuthenticated, (req, res) => {
  const create = {
    Name: req.body.name,
    Phone: req.body.phone,
    Alt: req.body.alt,
    Logo: req.body.logo,
    Sector: req.body.sector,
  };
  const options = {
    method: "POST",
    url: "http://localhost:1337/UsefullNumbers/",
    headers: { "Content-Type": "application/json" },
    data: create,
  };

  axios
    .request(options)
    .then(function (response) {
      res.render("./Numeros/addNumeros.ejs", { data: response.data });
    })
    .catch(function (error) {
      console.error(error);
    });
});
app.get("/updateNumeros/:id", checkAuthenticated, (req, res) => {
  const options = {
    method: "GET",
    url: `http://localhost:1337/UsefullNumbers/${req.params.id}`,
    headers: { "Content-Type": "application/json" },
  };

  axios
    .request(options)
    .then(function (response) {
      res.render("./Numeros/updateNumeros.ejs", { data: response.data, name: req.user.Name });
    })
    .catch(function (error) {
      console.error(error);
    });
});
app.post("/updateNumeros/:id", checkAuthenticated, (req, res) => {
  const update = {
    Name: req.body.name,
    Phone: req.body.phone,
    Alt: req.body.alt,
    Logo: req.body.logo,
    Sector: req.body.sector,
  };
  const options = {
    method: "PUT",
    url: `http://localhost:1337/UsefullNumbers/${req.params.id}`,
    headers: { "content-Type": "application/json" },
    data: update,
  };
  axios
    .request(options)
    .then(function (response) {
      res.render("./Numeros/updateNumeros.ejs", { data: response.data });
    })
    .catch(function (error) {
      console.error(error);
    });
});
app.get("/numeros/delete/:id", checkAuthenticated, (req, res) => {
  const options = {
    method: "DELETE",
    url: `http://localhost:1337/UsefullNumbers/${req.params.id}`,
    headers: { "Content-Type": "application/json" },
  };

  axios
    .request(options)
    .then(function (response) {
      res.render("./Numeros/deleteNumeros.ejs", { data: response.data });
    })
    .catch(function (error) {
      console.error(error);
    });
});

app.get("/carte", checkAuthenticated, (req, res) => {
  const readPlaces = JSON.parse(fs.readFileSync("./json/places.json"));

  res.render("./Carte/carte.ejs", { places: readPlaces });
});
app.get("/carte/create", checkAuthenticated, (req, res) => {
  res.render("./Carte/addPlaces.ejs", {name: req.user.Name});
});

app.post("/carte/create", checkAuthenticated, (req, res) => {
  const create = {
    Category: req.body.category,
    Name: req.body.name,
    Adress: req.body.adress,
    Phone: req.body.phone,
    Email: req.body.email,
    Timetable: req.body.timetable,
    Description: req.body.description,
    Weblink: req.body.weblink,
    Latitude: req.body.latitude,
    Longitude: req.body.longitude
  };
  const options = {
    method: "POST",
    url: "http://localhost:1337/Places/",
    headers: { "Content-Type": "application/json" },
    data: create,
  };

  axios
    .request(options)
    .then(function (response) {
      res.render("./Carte/addPlaces.ejs", { data: response.data, name: req.user.Name });
    })
    .catch(function (error) {
      console.error(error);
    });
});
app.get("/updatePlaces/:id", checkAuthenticated, (req, res) => {
  const options = {
    method: "GET",
    url: `http://localhost:1337/Places/${req.params.id}`,
    headers: { "Content-Type": "application/json" },
  };

  axios
    .request(options)
    .then(function (response) {
      res.render("./Carte/updatePlaces.ejs", { data: response.data, name: req.user.Name });
    })
    .catch(function (error) {
      console.error(error);
    });
});
app.post("/updatePlaces/:id", checkAuthenticated, (req, res) => {
  const update = {
    Name: req.body.name,
    Category: req.body.category,
    Adress: req.body.adress,
    Phone: req.body.phone,
    Email: req.body.email,
    Description: req.body.description,
    Weblink: req.body.weblink,
    Latitude: req.body.latitude,
    Longitude: req.body.longitude
  };
  const options = {
    method: "PUT",
    url: `http://localhost:1337/UsefullPlaces/${req.params.id}`,
    headers: { "content-Type": "application/json" },
    data: update,
  };
  axios
    .request(options)
    .then(function (response) {
      res.render("./Carte/updatePlaces.ejs", { data: response.data });
    })
    .catch(function (error) {
      console.error(error);
    });
});
app.get("/places/delete/:id", checkAuthenticated, (req, res) => {
  const options = {
    method: "DELETE",
    url: `http://localhost:1337/Places/${req.params.id}`,
    headers: { "Content-Type": "application/json" },
  };

  axios
    .request(options)
    .then(function (response) {
      res.render("./Carte/deletePlaces.ejs", { data: response.data });
    })
    .catch(function (error) {
      console.error(error);
    });
});
/* set up root "/login" and use checkNotAuthenticated to check authentification of users
 if the user is not authentificated redirect to login */

 app.get("/langues", checkAuthenticated, (req, res) => {
  const readLangues = JSON.parse(fs.readFileSync("./json/langues.json"));
  res.render("./Langues/langues.ejs", { Langues: readLangues, name: req.user.Name});
});
app.get("/langues/create", checkAuthenticated, (req, res) => {
  res.render("./Langues/addLangues.ejs", {name: req.user.Name});
});
app.post("/langues/create", checkAuthenticated, (req, res) => {
  const create = {
    Name: req.body.name,
    Initial: req.body.initial,
  };
  const options = {
    method: "POST",
    url: "http://localhost:1337/Languages/",
    headers: { "Content-Type": "application/json" },
    data: create,
  };

  axios
    .request(options)
    .then(function (response) {
      res.render("./Langues/addLangues.ejs", { data: response.data, name: req.user.Name });
    })
    .catch(function (error) {
      console.error(error);
    });
});
app.get("/updateLangues/:id", checkAuthenticated, (req, res) => {
  const options = {
    method: "GET",
    url: `http://localhost:1337/Languages/${req.params.id}`,
    headers: { "Content-Type": "application/json" },
  };

  axios
    .request(options)
    .then(function (response) {
      res.render("./Langues/updateLangues.ejs", { data: response.data, name: req.user.Name });
    })
    .catch(function (error) {
      console.error(error);
    });
});
app.post("/updateLangues/:id", checkAuthenticated, (req, res) => {
  const update = {
    Name: req.body.name,
    Initial: req.body.initial,
  };
  const options = {
    method: "PUT",
    url: `http://localhost:1337/Languages/${req.params.id}`,
    headers: { "content-Type": "application/json" },
    data: update,
  };
  axios
    .request(options)
    .then(function (response) {
      res.render("./Langues/updateLangues.ejs", { data: response.data, name: req.user.Name});
    })
    .catch(function (error) {
      console.error(error);
    });
});

app.get("/words", checkAuthenticated, (req, res) => {
  const readWords = JSON.parse(fs.readFileSync("./json/words.json"));
  res.render("./Langues/words.ejs", { Words: readWords, name: req.user.Name});
});
app.get("/words/create", checkAuthenticated, (req, res) => {
  res.render("./Langues/addWords.ejs", {name: req.user.Name});
});
app.post("/words/create", checkAuthenticated, (req, res) => {
  const create = {
    Name: req.body.name,
    Role: req.body.role,
    Language: req.body.language
  };
  const options = {
    method: "POST",
    url: "http://localhost:1337/Words/",
    headers: { "Content-Type": "application/json" },
    data: create,
  };

  axios
    .request(options)
    .then(function (response) {
      res.render("./Langues/addWords.ejs", { data: response.data, name: req.user.Name });
    })
    .catch(function (error) {
      console.error(error);
    });
});
app.get("/updateWords/:id", checkAuthenticated, (req, res) => {
  const options = {
    method: "GET",
    url: `http://localhost:1337/Words/${req.params.id}`,
    headers: { "Content-Type": "application/json" },
  };

  axios
    .request(options)
    .then(function (response) {
      res.render("./Langues/updateWords.ejs", { data: response.data, name: req.user.Name });
    })
    .catch(function (error) {
      console.error(error);
    });
});
app.post("/updateWords/:id", checkAuthenticated, (req, res) => {
  const update = {
    Name: req.body.name,
    Role: req.body.role,
    Language: req.body.language,
  };
  const options = {
    method: "PUT",
    url: `http://localhost:1337/Words/${req.params.id}`,
    headers: { "content-Type": "application/json" },
    data: update,
  };
  axios
    .request(options)
    .then(function (response) {
      res.render("./Langues/updateWords.ejs", { data: response.data, name: req.user.Name});
    })
    .catch(function (error) {
      console.error(error);
    });
});

app.get("/words/delete/:id", checkAuthenticated, (req, res) => {
  const options = {
    method: "DELETE",
    url: `http://localhost:1337/Words/${req.params.id}`,
    headers: { "Content-Type": "application/json" },
  };

  axios
    .request(options)
    .then(function (response) {
      res.render("./Langues/deleteWords.ejs", { data: response.data, name: req.user.Name });
    })
    .catch(function (error) {
      console.error(error);
    });
});

app.get("/login", checkNotAuthenticated, (req, res) => {
  res.render("login.ejs");
});
// set up the root /login to verificated user if succes redirect to "/" else redirect to "/login"
app.post(
  "/login",
  checkNotAuthenticated,
  passport.authenticate("local", {
    successRedirect: "/",
    failureRedirect: "/login",
    failureFlash: true,
  })
);
/* set up root "/register" and use checkNotAuthenticated to check authentification of users
 if the user is not authentificated redirect to register */
app.get("/register", checkNotAuthenticated, (req, res) => {
  res.render("register.ejs");
});
/* set up the root /register to save in bdd the user name, email and password (password be hash by bcrypt)
 redirect to "/" else redirect to "/login" */

app.post("/register", checkNotAuthenticated, async (req, res) => {
  try {
    // use bcrypt to hash password
    const hashedPassword = await bcrypt.hash(req.body.password, 10);
    // mysql request to insert user in bdd
    let sql = "INSERT INTO users (Name, Email, Password) VALUES ?";
    // get name, email and password from our form
    let values = [[req.body.name, req.body.email, hashedPassword]];
    db.query(sql, [values], function (err, result) {
      if (err) throw err;
      console.log("Number of records inserted: " + result.affectedRows);
    });
    // if succes redirect to "/login"
    res.redirect("/login");
  } catch {
    // if error redirect "/register"
    res.redirect("/register");
  }
});

// set up the root for log out
app.get("/logout", (req, res) => {
  req.logOut();
  res.redirect("/login");
});
// set up the function to check the users
function checkAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }

  res.redirect("/login");
}
// set up the function to check the user is not authentificated
function checkNotAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return res.redirect("/");
  }
  next();
}

// set up the apply to the 3001 port

app.listen(8003);
