let map = L.map('mapid').setView([43.1067395, 0.7259844], 15.5);

let OpenStreetMap_France = L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
	maxZoom: 20,
	attribution: '&copy; OpenStreetMap France | &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
});

map.addLayer(OpenStreetMap_France);

let markersCluster = new L.MarkerClusterGroup();

let data = parsed_data;
const text = document.getElementById("text")

for (let i = 0; i < data.length; i++) {
    console.log(window.location.href)

    
    if(window.location.hash.endsWith(data[i].Category) || window.location.hash === ''){
    let latLng = new L.LatLng(data[i].Latitude, data[i].Longitude);
    let marker = new L.Marker(latLng).bindPopup("Nom : " + data[i].Name + "<br/> Adresse : " + data[i].Adress + "<br/> Téléphone : " + data[i].Phone)
    .openPopup();;
    markersCluster.addLayer(marker);
    if(text === null){}else{
    let divText = document.createElement("div")
    let title = document.createElement("h2")
    let adress = document.createElement("p")
    let phone = document.createElement("p")
    let update = document.createElement("a")
    let deleteA = document.createElement("a")
    let updateButton = document.createElement("BUTTON")
    let deleteButton = document.createElement("BUTTON")
    let titleContent = document.createTextNode(data[i].Name)
    let adressContent = document.createTextNode(data[i].Adress)
    let phoneContent = document.createTextNode(data[i].Phone)
    let updateButtonContent = document.createTextNode("modifier")
    let deleteButtonContent = document.createTextNode("supprimer")
    title.appendChild(titleContent)
    adress.appendChild(adressContent)
    phone.appendChild(phoneContent)
    updateButton.appendChild(updateButtonContent)
    deleteButton.appendChild(deleteButtonContent)
    divText.appendChild(title)
    divText.appendChild(adress)
    divText.appendChild(phone)
    update.appendChild(updateButton)
    divText.appendChild(update)
    deleteA.appendChild(deleteButton)
    divText.appendChild(deleteA)
    text.appendChild(divText)
    update.setAttribute('href', `/updatePlaces/${data[i].Id}`)
    deleteA.setAttribute('href', `/places/delete/${data[i].Id}`)
    updateButton.setAttribute('class', 'all_button')
    deleteButton.setAttribute('class', 'all_button delete')
    divText.setAttribute("class", "div_text")
    
}}
    

}

map.addLayer(markersCluster);

